# Answers to the [DevOps Engineer test](https://www.notion.so/DevOps-Engineer-home-takeaway-Questions-9fa951ba11654ad5bd7a7f48cf12be6a)


## Description

### Setup `Kubernetes` using `Kind`, `Minikube`, or  [`k3s.io`](http://k3s.io/). 

I will go with the assumption that we already have a Kubernetes Cluster up and running whether being a production grade Kubernetes or just a single node development cluster. 

Otherwise, I could have created another git repository for cluster setup on existing OS installed machine using a tool like Ansible given the fact that you are running your workload OnPremise.

And if you want to automate OS rollout and update, I could have use Puppet Razor or Forman on your bare metal infrastructure.

### Create a `helloworld` application in any language of your choice.

I created a __Hello IREMBO__ golang app located in [main.go](./main.go)

#### Compiling
Go is a compiled programming language. That means, you can build you code and get the binary to run on any other machine with the same architecture like the machine where it was compible.
With that say you can have you executable away of your code.

That is why I will use Docker multistage build to make the final docker image less heavy. 

This will build our code
```
FROM golang:1.15-alpine AS build

WORKDIR /app

COPY . .

# Build the application
RUN go build -o main
```
I copy the binary into an empty less heavy based image to get our final image to run in Kuberntes
```
FROM alpine

WORKDIR /build

COPY --from=build /app/main .

CMD ./main
```

The final Dockerfile can be seen [here](./Dockerfile)

Here is the link to the public Docker file [Infrastructure Lead Docker image on Docker Hub](https://hub.docker.com/r/williamdekou/infrastructure-lead)

### and deploy that in a `kubernetes` cluster you have created using helm chart.

My setting for working with helm and Kubernetes is `helm:3.3.1`

```
# Created a Helm chart using 
$ helm create infrastructure-lead-helm
```

 Helm chart created this way comes with a boilerplate (deployment, service, horizontal pod autoscaler, service account and ingress objects). But for the seek of this exercice, we are only going to configure Kubernetes Deployments, Service, and Horizontal Pod Scaler

### Deployment
I specify those in the [Values.yml](./infrastructure-lead-helm/values.yaml) file : 
- Image repo to be [williamdekou/infrastructure-lead](https://hub.docker.com/r/williamdekou/infrastructure-lead) 
- Tag to be `BRANCH-latest` where BRANCH be either master for production or develop for development depending on the environnement in which we are roing
- pullPolicy to `Always` so that the Kubelet always downloads the latest image update not the cached ones

If the docker image was on a private docker registry, I will had to specify credentials to that Docker registry in imagePullSecrets

I defined more value in the [Values.yml](./infrastructure-lead-helm/values.yaml) file : 
- `containerPort` to  80 by default my docker image run on 8080 so I switch it to 80 and pass that same value as env to the docker image by tweaking the [Deployment file](./infrastructure-lead-helm/templates/deployment.yaml) with 

```
ports:
    - name: http
      containerPort: {{ .Values.containerPort }}
      protocol: TCP
env:
    - name: PORT
      value: {{ .Values.containerPort }}
```          
          
### The app should be able to autoscale at `50%` of resources.

In order to make autoscaling at 50% of `CPU` and `MEM` use, I set in [Values.yml](./infrastructure-lead-helm/values.yaml) file : 
```
autoscaling:
  enabled: true
  minReplicas: 3
  maxReplicas: 100
  targetCPUUtilizationPercentage: 50
  targetMemoryUtilizationPercentage: 50
```  

## The app should be able to be accessible in kubernetes DNS

Make my App accessible with Kubernetes DNS (`CoreDNS` underwood) will require me to create a Service. That DNS with a ClusterIP type will make sure the service is only visible within the Kubernetes. That says, It is more secure because not being expose to external attacks. 

That service DNS Record is  `infrastructure-lead` set to the fullnameOverride available in the  [Values.yml](./infrastructure-lead-helm/values.yaml) file

### Manage resources as you see fit for the pod that is running the app.

We settled to mange resource in other to inforce expected performance and reliability

Request management  within [Values.yml](./infrastructure-lead-helm/values.yaml) file : 
```
resources:
  limits:
    cpu: 200m
    memory: 256Mi
  requests:
    cpu: 100m
    memory: 128Mi
```
Here is both the requested and the upper limit that my container could use. This is almost equal to what the pod needs because this pod only has only one container. See in the [Deployment file](./infrastructure-lead-helm/templates/deployment.yaml). I could have specify this at the pod level too.

The requested resources are the mimimal resources to be hold by a node in other to be consider by kube-scheduler as one eligible schedule node for this pod. The limit is the upper front that the pod could use on a node if available.  

### Deploy the app in deferent namespace than `default`
I prefer not to specify the namespace in the chart because I could switch it as I am in different environment: production, develop (This exercise could have been more close to a production grade deployment if we had specify the a minimal number of replicas for our app and implemented an appropriate deployment stragegy)
```
$ helm install infrastructure-lead ./infrastructure-lead-helm --create-namespace --namespace infrastructure-lead-BRANCH # where BRANCH be either develop or production
```

To install it from CD, I use 
```
$ helm update infrastructure-lead ./infrastructure-lead-helm --create-namespace --namespace infrastructure-lead-BRANCH --install # where BRANCH be either develop or production
```

### Deploy default `redis` chart in different namespace : The `cache` namespace
```
# Production configuration 
$ helm repo add bitnami https://charts.bitnami.com/bitnami # Add bitnami repo where redis is hosted in Helm repo list in 
$ helm install redis bitnami/redis --create-namespace --namespace cache  # Deploy redis in `cache` namespace
```
### Explain how would your app connect to it (It does not to actually connect to it you can explain the approach)
In order to connect at redis in a different namespace my app will use the full service DNS record which is 

- `redis-master.cache` or `redis-master.cache.svc` or `redis-master.cache.svc.cluster.local` for read/write operations
- `redis-slave.cache` or `redis-slave.cache.svc` or `redis-slave.cache.svc.cluster.local` for read-only operations


In the [Values.yml](./infrastructure-lead-helm/values.yaml) file :

```
env:
  # I could use one of those link redis.cache or redis.cache.svc redis.cache.svc.cluster.local
  redisWriteLink: redis-master.cache.svc.cluster.local
  redisReadLink: redis-slave.cache.svc.cluster.local

```

In the [Deployment file](./infrastructure-lead-helm/templates/deployment.yaml), I passed those value to my container this way so that my app can use those variable to cache data with redis 
```
env:
    - name: REDIS_WRITE_LINK
      value: {{ .Values.env.redisWriteLink }}
    - name: REDIS_READ_LINK
      value: {{ .Values.env.redisReadLink }}

```


Please, check my CI/CD file here [.gitlab-ci.yml](./.gitlab-ci.yml) to see how I automated docker image build and release and Continuous deployment 