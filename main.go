package main

import (
    "fmt"
    "net/http"
    "os"
)

func main() {
    port := getEnv("PORT",  "8080")

    http.HandleFunc("/", HelloServer)
    http.ListenAndServe(":" + port , nil)
}

func HelloServer(w http.ResponseWriter, r *http.Request) {
    fmt.Fprintf(w, "Hello IREMBO!")
}

func getEnv(key string, defaultVal string) string {
    if value, ok := os.LookupEnv(key); ok {
	return value
    }

    return defaultVal
}
