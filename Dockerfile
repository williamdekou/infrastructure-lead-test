FROM golang:1.15-alpine AS build

WORKDIR /app

COPY . .

# Build the application
RUN go build -o main

# Using multistage build to size down the image as the build container generate a binary
FROM alpine

WORKDIR /build

COPY --from=build /app/main .

CMD ./main